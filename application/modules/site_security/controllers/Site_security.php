<?php
class Site_security extends MX_Controller 
{

function __construct() {
parent::__construct();
}

/*
function test()

// Secure PHP Password Hashing: What is a hash? - https://www.youtube.com/watch?v=ay1kBltU6Ck 
{
    $name = "David";
    $hashed_name = $this->_hash_string($name);
    echo "You are $name<br>";
    echo $hashed_name;
    
    echo"<br>";
    $submitted_name = "David";
    $result = $this->_verify_hash($submitted_name, $hashed_name);
    
    if ($result==TRUE) {
        echo "well done";
    } else {
        echo "fail";
    }
}
*/

function _hash_string($str)
{
    $hashed_string = password_hash($str, PASSWORD_BCRYPT, array(
        
        'cost' =>11
    ));
    
    return $hashed_string;
}

function _verify_hash($plain_text_str, $hashed_string)

{
    $result = password_verify($plain_text_str, $hashed_string);
    return $result; //TRUE or FALSE 
}


function _make_sure_is_admin()
{
    $is_admin = TRUE;
    
    if ($is_admin!=TRUE) {
        redirect('site_security/not_allowed');
    }
}

function not_allowed()
{
    echo "You are not allowed to be here.";
}

}