<?php
class Store_items extends MX_Controller 
{

function __construct() {
parent::__construct();
//Codeigniter 3-tol kell a betoltes
$this->load->library('form_validation');
$this->form_validation->CI =& $this;
}

function _get_item_id_from_item_url($item_url)
{
    $query =$this->get_where_custom('item_url', $item_url);
    foreach($query->result() as $row) {
        $item_id = $row->id;
    }
    
    if (!isset($item_id)) {
        $item_id = 0;
    }
    
    return $item_id;
}

function view($update_id)
{
/************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
/************** Biztonsagi funkciok vege *******************/  

    //fetch the item details
    $data = $this->fetch_data_from_db($update_id);
    $data['update_id'] = $update_id;
    $data['flash']= $this->session->flashdata('item');
    $data['view_module'] = "store_items";
    $data['view_file'] = "view";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);

}

function _process_delete($update_id)
{
    //attempt to delete item colours
    $this->load->module('store_item_colours');
    $this->store_item_colours->_delete_for_item($update_id);
    
    //attempt to delete item sizes
    
    $this->load->module('store_item_sizes');
    $this->store_item_sizes->_delete_for_item($update_id);
   
    //attempt to delete big pics/small pics
    $data = $this->fetch_data_from_db($update_id);
    $big_pic = $data['big_pic'];
    $small_pic = $data['small_pic'];
    
    $big_pic_path = './big_pics/'.$big_pic;
    $small_pic_path = './small_pics/'.$small_pic;
    
    //attempt to remove the images
    if (file_exists($big_pic_path)) {
        unlink($big_pic_path); //toroljuk a kepet
    } 
    
    if (file_exists($smallpic_path)) {
        unlink($small_pic_path);
    } 
    
    //delete the item record from store_items
    $this->_delete($update_id);
}

function delete($update_id)

{
/************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
/************** Biztonsagi funkciok vege *******************/  

    $submit = $this->input->post('submit', TRUE);
    if ($submit=="Cancel") {
        redirect ('store_items/create/'.$update_id);
    } elseif ($submit=="Yes - Delete Item") {
        $this->_process_delete($update_id);
        
/***************** flash message ********************/               
    $flash_msg = "The item image was successfully deleted.";
    $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>'; 
    $this->session->set_flashdata('item', $value);
 /************** flash message end ********************/     
 
    redirect('store_items/manage'); 
    }
}

function deleteconf($update_id)
{
/************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
/************** Biztonsagi funkciok vege *******************/  

    $data['headline'] = "Delete Item";
    $data['update_id'] = $update_id;
    $data['flash']= $this->session->flashdata('item');
    $data['view_file'] = "deleteconf";
    $this->load->module('templates');
    $this->templates->admin($data);
}


function delete_image($update_id) 
{
    
/************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
/************** Biztonsagi funkciok vege *******************/  

    $data = $this->fetch_data_from_db($update_id);
    $big_pic = $data['big_pic'];
    $small_pic = $data['small_pic'];
    
    $big_pic_path = './big_pics/'.$big_pic;
    $small_pic_path = './small_pics/'.$small_pic;
    
    //attempt to remove the images
    if (file_exists($big_pic_path)) {
        unlink($big_pic_path); //toroljuk a kepet
    } 
    
    if (file_exists($smallpic_path)) {
        unlink($small_pic_path);
    } 
    
    //update the database
    unset($data);
    $data['big_pic'] ="";
    $data['small_pic'] ="";
    $this->_update($update_id, $data);
    
    //flash message
    $flash_msg = "The item image was successfully deleted.";
    $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>'; 
    $this->session->set_flashdata('item', $value);
    
    redirect('store_items/create/'.$update_id);
}

function _generate_thumbnail($file_name) //Image Manipulation Class - Processing an Image on Codeigniter Guide
{
$config['image_library'] = 'gd2';
$config['source_image'] = './big_pics/'.$file_name;
$config['new_image'] = './small_pics/'.$file_name;
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 200;
$config['height']       = 200;

$this->load->library('image_lib', $config);

$this->image_lib->resize();
}

function do_upload($update_id) //codeigniter guide: Controller
{
    
/************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
/************** Biztonsagi funkciok vege *******************/   
//Cancel
    $submit = $this->input->post('submit', TRUE);
    if ($submit=="Cancel") {
        redirect('store_items/create/'.$update_id);
    }

    $config['upload_path']          = './big_pics/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 1000;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('userfile'))
    {
            $data['error'] = array('error' => $this->upload->display_errors("<p style='color: red;'>", "</p>"));
            
            /*foreach ($error as $key => $value) {
                echo $value."<br>";
            }*/
            
             $data['headline'] = "Upload Error";
             $data['update_id'] = $update_id;
             $data['flash']= $this->session->flashdata('item');
             $data['view_file'] = "upload_image";
             $this->load->module('templates');
             $this->templates->admin($data);
    
    }
    else
    {        
            //upload was successful
             $data = array('upload_data' => $this->upload->data());
             
             $upload_data= $data['upload_data'];
             $file_name = $upload_data['file_name'];
             $this->_generate_thumbnail($file_name);
             
             //update the database
             $update_data['big_pic'] = $file_name;
             $update_data['small_pic'] = $file_name;
             $this-> _update($update_id, $update_data);
             
             $data['headline'] = "Upload Success";
             $data['update_id'] = $update_id;
             $data['flash']= $this->session->flashdata('item');
             $data['view_file'] = "upload_success";
             $this->load->module('templates');
             $this->templates->admin($data);
             
    }
        
}

function upload_image($update_id) 
{
    
 /************ Biztonsagi funkciok betoltese ******************/    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
/***************** Biztonsagi funkciok vege ******************/   
    
    
    $data['headline'] = "Upload Image";
    $data['update_id'] = $update_id;
    $data['flash']= $this->session->flashdata('item');
    $data['view_file'] = "upload_image";
    $this->load->module('templates');
    $this->templates->admin($data);
}


//item creater létrehozása

function create() 
{

    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
    //headline betöltése
    
    $update_id = $this->uri->segment(3);
    
    
    /*This provides you to retrieve information from your URI strings

    $this->uri->segment(n); // n=1 for controller, n=2 for method, etc
    
    it will return
    
    $this->uri->segment(1); // controller
    $this->uri->segment(2); // action
    $this->uri->segment(3); // 1stsegment
    $this->uri->segment(4); // 2ndsegment 
    
    Bővebben: https://www.codeigniter.com/userguide3/libraries/uri.html */
    
    $submit = $this->input->post('submit', TRUE);
    //cancel gomb    
    if($submit =="Cancel") {
        redirect('store_items/manage');
    }
    //submit gomb
    elseif ($submit=="Submit") {
        //process the form , validacio beallitasa a form-hoz
        $this->load->library('form_validation');
        $this->form_validation->set_rules('item_title','Item Title','required|max_length[240]|callback_item_check'); //callback check: az itemeknek egzediknek kell lenniuk
        $this->form_validation->set_rules('item_price','Item Price','required|numeric');
        $this->form_validation->set_rules('was_price','Was Price','numeric');
        $this->form_validation->set_rules('status','Status','required|numeric');
        $this->form_validation->set_rules('item_description','Item Description','required');
        
        if ($this->form_validation->run() == TRUE) {  //Codeigniter 2 nel:   if ($this->form_validation->run($this) == TRUE) { 
            //get the variables
            //echo "well done"; die();
            $data = $this->fetch_data_from_post();
            $data['item_url'] = url_title($data['item_title']);
            
            
            if (is_numeric($update_id)) {
                
                //update the item details
                $this->_update($update_id, $data);
 /***************** flash message ********************/               
                $flash_msg = "The item details were successfully updated.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>'; 
                $this->session->set_flashdata('item', $value);
 /************** flash message end ********************/                   
                redirect('store_items/create/'.$update_id);
                
            } else {
                //insert a new item
               
                $this->_insert($data);
                $update_id=$this->get_max(); // get the ID of the new item
                $flash_msg = "The item was successfully added.";
                $value = '<div class="alert alert-sucess" role="alert">'.$flash_msg.'</div>'; 
                $this->session->set_flashdata('item', $value);
                redirect('store_items/create/'.$update_id);
            }
        }
    }
    
    
    if ((is_numeric($update_id)) && ($submit!="Submit")) {
        $data = $this->fetch_data_from_db($update_id);
    } else {
        $data = $this->fetch_data_from_post();
        $data['big_pic'] = "";
    }
    
    if (!is_numeric($update_id)) {
        $data ['headline'] = "Add New Item";
    } else {
        $data ['headline'] = "Update Item Details";
    }
//view file     
    $data['update_id'] = $update_id;
    $data['flash']= $this->session->flashdata('item');
   // $data ['flash'] = '<div class="alert alert-sucess" role="alert">All is well</div>';  //flash data
    
    $data['view_file'] = "create";
    $this->load->module('templates');
    $this->templates->admin($data);
}

//értékek adása a formnak

function fetch_data_from_post()
{
    $data['item_title'] = $this->input->post('item_title', TRUE);
    $data['item_price'] = $this->input->post('item_price', TRUE);
    $data['was_price'] = $this->input->post('was_price', TRUE);
    $data['item_description'] = $this->input->post('item_description', TRUE);
    $data['status'] = $this->input->post('status', TRUE);
    
    return $data;
}

function fetch_data_from_db($update_id) 
{
    
    if (!is_numeric($update_id)) {
        redirect('site_security/not_allowed');
    }
    
    $query = $this->get_where($update_id);
    foreach($query->result() as $row) {
        $data['item_title'] = $row->item_title;
        $data['item_url'] = $row->item_url;
        $data['item_price'] = $row->item_price;
        $data['item_description'] = $row->item_description;
        $data['big_pic'] = $row->big_pic;
        $data['small_pic'] = $row->small_pic;
        $data['was_price'] = $row->was_price;
        $data['status'] = $row->status;
    }
    
    if (!isset($data)) {
        $data  ="";
    }
    
    return $data;
    
}

//item manager létrehozása, csak admin joggal

function manage() 
{
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    
    $data['flash']= $this->session->flashdata('item');
    
//view file  
    $data ['query'] = $this->get('item_title');  //manage datatable
    $data['view_file'] = "manage";
    $this->load->module('templates');
    $this->templates->admin($data);
}

function get($order_by)
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get($order_by);
    return $query;
}

//test
function get_with_limit($limit, $offset, $order_by) 
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_where($id);
    return $query;
}

function get_where_custom($col, $value) 
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_delete($id);
}

function count_where($column, $value) 
{
    $this->load->model('mdl_store_items');
    $count = $this->mdl_store_items->count_where($column, $value);
    return $count;
}

function get_max() 
{
    $this->load->model('mdl_store_items');
    $max_id = $this->mdl_store_items->get_max();
    return $max_id;
}

function _custom_query($mysql_query) 
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->_custom_query($mysql_query);
    return $query;
}

//callback item check - egyedinek kell lenniuk !!!

function item_check($str) 
{
    
    $item_url = url_title($str);
    $mysql_query = "SELECT * FROM store_items WHERE item_title='$str' AND item_url='$item_url'";
    
    $update_id = $this->uri->segment(3);
    if (is_numeric($update_id)) {
        //this is an update
        $mysql_query.=" AND id!=$update_id";
    } 
    
    $query = $this->_custom_query($mysql_query);
    $num_rows = $query->num_rows();
    
    if ($num_rows>0)
    {
        $this->form_validation->set_message('item_check', 'The item title that you submitted is not available');
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

}