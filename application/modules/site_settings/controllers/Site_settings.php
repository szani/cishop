<?php
class Site_settings extends MX_Controller 
{

function __construct() {
parent::__construct();
}

function _get_item_segments()
{
   // return the segments for the store_item page (produce page)
   $segments = "musical/instrument/";
   return $segments;
}

function _get_items_segments()
{
   // return the segments for the category pages
   $segments = "music/instruments/";
   return $segments;
}

}